﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalZoo
{
    abstract class Animal
    {
        public string Name { get; set; }
        public double Weight { get; set; }
        public string TypeOfAnimal { get; set; }
        public Animal(string name, double weight)
        {
            Name = name;
            Weight = weight;
        }

        public abstract void Eat();

        public abstract string ToString();
    }
}

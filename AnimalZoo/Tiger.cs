﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalZoo
{
    class Tiger : Animal
    {
        public Tiger(string n, double w) : base(n, w)
        {
            TypeOfAnimal = "Tiger";
        }
        public override void Eat()
        {
            Weight += 1;
            Console.WriteLine("Weight increased by 1kg");
        }

        public override string ToString()
        {
            return "Animal: " + TypeOfAnimal +
                " | Name: " + Name +
                " | Weight: " + Weight +"kg";
        }
    }
}

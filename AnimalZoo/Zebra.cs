﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalZoo
{
    class Zebra : Animal
    {
        public Zebra(string name, double weight) : base(name, weight)
        {
            TypeOfAnimal = "Zebra";
        }

        public override void Eat()
        {
            Weight += 0.5;
            Console.WriteLine("Weight increased by 0.5kg");
        }

        public override string ToString()
        {
            return "Animal: " + TypeOfAnimal +
                " | Name: " + Name +
                " | Weight: " + Weight + "kg";
        }
    }
}

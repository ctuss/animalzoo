﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalZoo
{
    class Elephant : Animal
    {
        public Elephant(string name, double weight) : base(name, weight)
        {
            TypeOfAnimal = "Elephant";
        }
        public override void Eat()
        {
            Weight += 1.5;
            Console.WriteLine("Weight increased by 1.5kg");
        }

        public override string ToString()
        {
            return "Animal: " + TypeOfAnimal +
                " | Name: " + Name + 
                " | Weight: " + Weight +"kg";
        }
    }
}

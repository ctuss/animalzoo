﻿using System;
using System.Collections.Generic;

namespace AnimalZoo
{
    class Program
    {
        static void Main(string[] args)
        {
            bool doContinue = true;
            string input = "";
            int decision;
            List<Animal> animalList = new List<Animal>();
            Console.WriteLine("Hello and Welcome to AnimalZoo!");

            do
            {
                Console.WriteLine("-----------------------------" +
                    "\nSee all animals; Enter 1" +
                    "\nAdd animal to zoo; Enter 2" +
                    "\nExit zoo; Enter 0");
                input = Console.ReadLine();
                decision = Convert.ToInt32(input);

                switch (decision)
                {
                    case 1:
                        Console.WriteLine("There's currently " + animalList.Count + " animals in the zoo!");
                        Console.WriteLine(PrintAllAnimals(animalList));
                        break;
                    case 2:
                        CreateAnimal(animalList);
                        break;
                    default:
                        doContinue = false;
                        break;
                }

            } while (doContinue);
        }
        static bool CreateAnimal(List<Animal> list)
        {
            string input;
            string inputName;
            string inputWeight;
            double weightOfAnimal;

            while (true)
            {
                Console.WriteLine("Do you wish to add a tiger, elephant or zebra?");
                input = Console.ReadLine();
                if (input.Contains("tiger") || input.Contains("elephant") || input.Contains("zebra"))
                {
                    Console.WriteLine("What is the desired name?");
                    inputName = Console.ReadLine();
                    Console.WriteLine("What is the weight of the animal?");
                    inputWeight = Console.ReadLine();
                    weightOfAnimal = Convert.ToDouble(inputWeight);

                    if (input == "tiger")
                    {
                        list.Add(new Tiger(inputName, weightOfAnimal));
                        Console.WriteLine("Successfully added!");
                        return true;
                    }
                    else if (input == "elephant")
                    {
                        list.Add(new Elephant(inputName, weightOfAnimal));
                        Console.WriteLine("Successfully added!");
                        return true;
                    }
                    else if (input == "zebra")
                    {
                        list.Add(new Zebra(inputName, weightOfAnimal));
                        Console.WriteLine("Successfully added!");
                        return true;
                    }
                }
            }
        }
        static string PrintAllAnimals(List<Animal> list)
        {
            string output = "";
            list.ForEach(delegate (Animal animal) {
                output += animal.ToString() + "\n";
            });

            return output;
        }
    }
}
